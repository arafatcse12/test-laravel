<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/second-buyer-eloquent','DiaryTakenController@allTotalNumbers');
Route::get('/second-buyer-no-eloquent','DiaryTakenController@second_buyer_no_eloquent');

Route::get('/record-transfer','DiaryTakenController@record_transfer');
Route::get('/purchase-list-eloquent','DiaryTakenController@purchase_list_eloquent');
Route::get('/purchase-list-no-eloquent','DiaryTakenController@purchase_list_no_eloquent');
Route::get('/define-callback-js','DiaryTakenController@define_callback_js');
Route::get('/sort-js','DiaryTakenController@sort_js');
Route::get('/foreach-js','DiaryTakenController@foreach_js');
Route::get('/filter-js','DiaryTakenController@filter_js');
Route::get('/map-js','DiaryTakenController@map_js');
Route::get('/reduce-js','DiaryTakenController@reduce_js');
Route::get('/i-m-funny','DiaryTakenController@i_m_funny');

