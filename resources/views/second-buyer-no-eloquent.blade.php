<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Itech Test</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 600;
            height: 100vh;
            margin: 0;
            padding: 10px;
        }
        table{
            margin-left: 45px;
        }

        td{
            border: 1px solid #eee;
            padding: 3px;
            text-align: center;
        }
        code{
            color: #007b00;
        }
        a{
            color: red;
        }
    </style>
</head>
<body>
    <div class="content">
       
        <table>
            <tr>
                <td>Buyer id</th>
                <td>Buyer Name</th>
                <td>Total Diary Taken</th>
                <td>Total Pen Taken</th>
                <td>Total Eraser Taken</th>
                <td>Total items Taken</th>
            </tr>
            <tr>
                <td>{{ $dairy->buyer_id }}</th>
                <td>{{ $dairy->name }}</th>
                <td>{{ $dairy->totalDairy }}</th>
                <td>{{ $pen->totalPen }}</th>
                <td>{{ $eraser->totalEraser }}</th>
                <td>{{ $totalItems }}</th>
            </tr>
            <tr>
                <td>{{ $seconddairy->buyer_id }}</th>
                <td>{{ $seconddairy->name }}</th>
                <td>{{ $seconddairy->totalDairy }}</th>
                <td>{{ $secondPen->totalPen }}</th>
                <td>{{  $secondEraser->totalEraser }}</th>
                <td>{{ $totalItems1 }}</th>
            </tr>
            <tr>
                <td>{{ $seconddairy1->buyer_id }}</th>
                <td>{{ $seconddairy1->name }}</th>
                <td>{{ $seconddairy1->totalDairy }}</th>
                <td>{{ $secondPen1->totalPen }}</th>
                <td>{{ $secondEraser1->totalEraser }}</th>
                <td>{{ $totalItems2 }}</th>
            </tr>
           
        </table>
       
        
    </div>
</body>
</html>
