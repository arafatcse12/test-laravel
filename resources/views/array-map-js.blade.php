<h3>square root of each element in the array </h3>

<button onclick="arrayMap()">Array Map</button>

<p id="mapID"></p>

<script>
var numbers = [25, 36, 49, 64];

function arrayMap() {
  arrayValues = document.getElementById("mapID")
  arrayValues.innerHTML = numbers.map(Math.sqrt);
}
</script>