<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Itech Test</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 600;
            height: 100vh;
            margin: 0;
            padding: 10px;
        }
        table{
            margin-left: 45px;
        }

        td{
            border: 1px solid #eee;
            padding: 3px;
            text-align: center;
        }
        code{
            color: #007b00;
        }
        a{
            color: red;
        }
    </style>
</head>
<body>
    <div class="content">
       
        <table>
            <tr>
                <td>Buyer Id</td>
                <td>Buyer Name</td>
                <td>Total Diary Taken</td>
                <td>Total Pen Taken</td>
                <td>Total Eraser Taken</td>
                <td>Total items Taken</th>
            </tr>
            @foreach($collection->reverse() as $data)
                @foreach($data as $row)
                    @if($loop->index==0)
            <tr>
                <td>{{$row->buyer_id}}</td>
                <td>{{ $row->buyers->name}}</td>
                <td>{{ DB::table('diary_taken')->where('buyer_id',$row->buyer_id)->selectRaw('sum(amount) as total')->groupBy('buyer_id')->pluck('total')->first() }}</td>
                <td>{{ DB::table('pen_taken')->where('buyer_id',$row->buyer_id)->selectRaw('sum(amount) as total')->groupBy('buyer_id')->pluck('total')->first() }}</td>
                <td>{{ DB::table('eraser_taken')->where('buyer_id',$row->buyer_id)->selectRaw('sum(amount) as total')->groupBy('buyer_id')->pluck('total')->first() }}</td>
                
                <td>{{ $data->sum('total') }}</th> 
            </tr>
            @endif
            @endforeach
            @endforeach
            
        </table>
    </div>
</body>
</html>
