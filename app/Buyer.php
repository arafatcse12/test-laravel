<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $table='buyers';

    public function dairy()
    {
        return $this->hasMany('App\DiaryTaken','buyer_id','id');
    }

    public function eraser()
    {
        return $this->hasMany('App\EraserTaken','buyer_id','id');
    }

    public function pen()
    {
        return $this->hasMany('App\PenTaken','buyer_id','id');
    }

    public function totalDairy()
    {
        return $this->hasMany('App\DiaryTaken','buyer_id','id')->selectRaw('sum(amount) as total')->groupBy('buyer_id');
    }
    public function totalPen()
    {
        return $this->hasMany('App\PenTaken','buyer_id','id')->select(DB::raw('COALESCE(sum(amount),0) AS total'))->groupBy('buyer_id');
    }
    public function totalEraser()
    {
        return $this->hasMany('App\EraserTaken','buyer_id','id')->select(DB::raw('COALESCE(sum(amount),0) AS total'))->groupBy('buyer_id');
    }

    

   

    
}
