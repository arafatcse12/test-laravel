<?php

namespace App\Http\Controllers;

use App\Buyer;
use App\Record;
use App\PenTaken;
use App\DiaryTaken;
use App\EraserTaken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DiaryTakenController extends Controller
{

    public function allTotalNumbers(){

        $dairy = DiaryTaken::select('buyer_id',DB::raw('sum(amount) AS totalDairy'))->groupBy('buyer_id')->orderBy('totalDairy','DESC')->skip(1)->take(1)->first();

        $eraser = EraserTaken::select('buyer_id',DB::raw('sum(amount) AS totalEraser'))->groupBy('buyer_id')->where('buyer_id',$dairy->buyer_id)->first();

        $pen = PenTaken::select('buyer_id',DB::raw('sum(amount) AS totalPen'))->groupBy('buyer_id')->orderBy('totalPen','DESC')->where('buyer_id',$dairy->buyer_id)->first();

        $secondEraser = EraserTaken::select('buyer_id',DB::raw('sum(amount) AS totalEraser'))->groupBy('buyer_id')->orderBy('totalEraser','DESC')->skip(1)->take(1)->first();

        $seconddairy = DiaryTaken::select('buyer_id',DB::raw('sum(amount) AS totalDairy'))->groupBy('buyer_id')->orderBy('totalDairy','DESC')->where('buyer_id',$secondEraser->buyer_id)->first();
        // dd($eraser);
        $secondPen  = PenTaken::select('buyer_id',DB::raw('sum(amount) AS totalPen'))->groupBy('buyer_id')->orderBy('totalPen','DESC')->where('buyer_id',$secondEraser->buyer_id)->first();

        $secondPen1  = PenTaken::select('buyer_id',DB::raw('sum(amount) AS totalPen'))->groupBy('buyer_id')->orderBy('totalPen','DESC')->skip(1)->take(1)->first();
        $secondEraser1 = EraserTaken::select('buyer_id',DB::raw('sum(amount) AS totalEraser'))->groupBy('buyer_id')->orderBy('totalEraser','DESC')->where('buyer_id',$secondPen1->buyer_id)->first();

        $seconddairy1 = DiaryTaken::select('buyer_id',DB::raw('sum(amount) AS totalDairy'))->groupBy('buyer_id')->orderBy('totalDairy','DESC')->where('buyer_id',$secondPen1->buyer_id)->first();

        $totalItems  =  $dairy->totalDairy + $eraser->totalEraser + $pen->totalPen;
        $totalItems1  =  $seconddairy->totalDairy + $secondEraser->totalEraser + $secondPen->totalPen;
        $totalItems2 =  $seconddairy1->totalDairy + $secondEraser1->totalEraser + $secondPen1->totalPen;
       

        return view('second-buyer-eloquent',compact('dairy','eraser','pen','totalItems','totalItems1','totalItems2','seconddairy1','secondEraser1','secondPen1','secondPen','seconddairy','secondEraser'));

    
    }

    public function second_buyer_no_eloquent(){
        $dairy = DB::table('diary_taken')->join('buyers','diary_taken.buyer_id','=','buyers.id')->select('buyer_id','name',DB::raw('sum(amount) AS totalDairy'))->groupBy('buyer_id')->orderBy('totalDairy','DESC')->skip(1)->take(1)->first();

        $eraser = DB::table('eraser_taken')->join('buyers','eraser_taken.buyer_id','=','buyers.id')->select('buyer_id','name',DB::raw('sum(amount) AS totalEraser'))->groupBy('buyer_id')->where('buyer_id',$dairy->buyer_id)->first();

        $pen = DB::table('pen_taken')->join('buyers','pen_taken.buyer_id','=','buyers.id')->select('buyer_id','name',DB::raw('sum(amount) AS totalPen'))->groupBy('buyer_id')->orderBy('totalPen','DESC')->where('buyer_id',$dairy->buyer_id)->first();

        $secondEraser = DB::table('eraser_taken')->join('buyers','eraser_taken.buyer_id','=','buyers.id')->select('buyer_id','name',DB::raw('sum(amount) AS totalEraser'))->groupBy('buyer_id')->orderBy('totalEraser','DESC')->skip(1)->take(1)->first();

        $seconddairy = DB::table('diary_taken')->join('buyers','diary_taken.buyer_id','=','buyers.id')->select('buyer_id','name',DB::raw('sum(amount) AS totalDairy'))->groupBy('buyer_id')->orderBy('totalDairy','DESC')->where('buyer_id',$secondEraser->buyer_id)->first();
        // dd($eraser);
        $secondPen  =DB::table('pen_taken')->join('buyers','pen_taken.buyer_id','=','buyers.id')->select('buyer_id','name',DB::raw('sum(amount) AS totalPen'))->groupBy('buyer_id')->orderBy('totalPen','DESC')->where('buyer_id',$secondEraser->buyer_id)->first();

        $secondPen1  = DB::table('pen_taken')->join('buyers','pen_taken.buyer_id','=','buyers.id')->select('buyer_id','name',DB::raw('sum(amount) AS totalPen'))->groupBy('buyer_id')->orderBy('totalPen','DESC')->skip(1)->take(1)->first();

        $secondEraser1 = DB::table('eraser_taken')->join('buyers','eraser_taken.buyer_id','=','buyers.id')->select('buyer_id','name',DB::raw('sum(amount) AS totalEraser'))->groupBy('buyer_id')->orderBy('totalEraser','DESC')->where('buyer_id',$secondPen1->buyer_id)->first();

        $seconddairy1 = DB::table('diary_taken')->join('buyers','diary_taken.buyer_id','=','buyers.id')->select('buyer_id','name',DB::raw('sum(amount) AS totalDairy'))->groupBy('buyer_id')->orderBy('totalDairy','DESC')->where('buyer_id',$secondPen1->buyer_id)->first();

        $totalItems  =  $dairy->totalDairy + $eraser->totalEraser + $pen->totalPen;
        $totalItems1  =  $seconddairy->totalDairy + $secondEraser->totalEraser + $secondPen->totalPen;
        $totalItems2 =  $seconddairy1->totalDairy + $secondEraser1->totalEraser + $secondPen1->totalPen;
       

        return view('second-buyer-no-eloquent',compact('dairy','eraser','pen','totalItems','totalItems1','totalItems2','seconddairy1','secondEraser1','secondPen1','secondPen','seconddairy','secondEraser')); 
    }

    public function record_transfer(){
        $path =  Storage::disk('public')->get('records.json'); 
            $data = json_decode($path,true);
            // dd($data);
            foreach ($data as $key => $value) 
            {
                foreach ($value as $key => $row) 
                {
                    Record::insert([
                        'id'=>$row["id"],
                        'from_statement'=>$row["from_statement"],
                        'financial_instrument_code'=>$row["financial_instrument_code"],
                        'action'=>$row["action"],
                        'entry_price'=>$row["entry_price"],
                        'closed_price'=>$row["closed_price"],
                        'take_profit_1'=>$row["take_profit_1"],
                        'stop_loss_1'=>$row["stop_loss_1"],
                        'signal_result'=>$row["signal_result"],
                        'status'=>$row["status"],
                        'statement_batch'=>$row["statement_batch"],
                        'closed_on'=>$row["closed_on"],
                    ]);
                }
            }  
        return "Successfully inserted data";
    }

    public function purchase_list_eloquent(){

            $dairy = DiaryTaken::with('buyers')->selectRaw('buyer_id, COALESCE(sum(amount),0) total')
                  ->groupBy('buyer_id')
                  ->get();
             $pen = PenTaken::with('buyers')->selectRaw('buyer_id, COALESCE(sum(amount),0) total')
             ->groupBy('buyer_id')
             ->get();
             $eraser = EraserTaken::with('buyers')->selectRaw('buyer_id, COALESCE(sum(amount),0) total')
             ->groupBy('buyer_id')
             ->get();
            //  dd($dairy,$pen,$eraser);

 
         $dairys = $dairy->collect();
         $pen = $pen->collect();
         $eraser = $eraser->collect();

         $collection = $dairys->merge($pen)->merge($eraser)->groupBy('buyer_id');
         // dd($collection);
         
        return view('purchase-list-eloquent',compact('pen','eraser','dairy','collection'));
     } 

     public function purchase_list_no_eloquent(){
        $dairy = DB::table('buyers')
            ->join('diary_taken','buyers.id','=','diary_taken.buyer_id')
             ->selectRaw('buyers.id,buyers.name, COALESCE(sum(diary_taken.amount),0) total')
             ->groupBy('buyers.id')
             ->get();
            
         $pen = DB::table('buyers')
            ->join('pen_taken','buyers.id','=','pen_taken.buyer_id')
            ->select('buyers.id','buyers.name',DB::raw('COALESCE(sum(amount),0) AS total'))
            ->groupBy('buyers.id')
            ->get();
         $eraser = DB::table('buyers')
           ->join('eraser_taken','buyers.id','=','eraser_taken.buyer_id')
            ->select('buyers.id','buyers.name',DB::raw('COALESCE(sum(amount),0) AS total'))
            ->groupBy('buyers.id')
            ->get();
 
         $collection = $dairy->merge($pen)->merge($eraser)->groupBy('id')->collect();
        
        return view('purchase-list-no-eloquent',compact('collection','pen','eraser'));
     } 


     public function define_callback_js(){

        return view('define-callback-js');
     }

     public function sort_js(){
        
        return view('sort-js');
     }

     public function foreach_js(){
        
        return view('foreach-js');
     }

     public function filter_js(){
        
        return view('filter-js');
     }
     public function map_js(){
        
        return view('array-map-js');
     }
     public function reduce_js(){
        
        return view('reduce-js');
     }

      public function i_m_funny(){
        
        return view('i-m-funny');
     }
}
