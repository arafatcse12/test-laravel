<?php

namespace App;

use App\DiaryTaken;
use Illuminate\Database\Eloquent\Model;

class DiaryTaken extends Model
{
    protected $table='diary_taken';

    
    public function buyers()
    {
        return $this->belongsTo('App\Buyer','buyer_id','id');
    }

    public function buyersSingle()
    {
        return $this->belongsTo('App\Buyer','buyer_id','id');
    }

   

    

}
