<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenTaken extends Model
{
    protected $table='pen_taken';

    public function buyers()
    {
        return $this->belongsTo('App\Buyer','buyer_id','id');
    }
}
