<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EraserTaken extends Model
{
    protected $table='eraser_taken';

    public function buyers()
    {
        return $this->belongsTo('App\Buyer','buyer_id','id');
    }
}
